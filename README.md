# About
This project was born as a design experiment using the language features of
Scala with the goal of modularity, extensibility, reliability, distribution and
ease of development.  While this is starting fundamentally as a design
experiment, the hope is the work applied here will provide a solid foundation
for building fully functional and "enterprise grade" e-commerce components.

This project does not try to provide a fully functional "one size fits all"
online store.  Instead, this should provide a solid piece or two to integrate
with a well designed solution catered to your specific business domain.

The following components have been implemented:

## Measures

Support for common scalar values, particularly money, that have specific
rounding rules and potentially changing conversion rates between units of
measure.

## Pricing

A composable, extensible, high performance pricing library capable of providing
detailed messaging and full traceability of how prices were calculated.

For details, please view the Pricing module's README for more info.

# License
This project is Open Source and available under the Apache 2 License.
