package matlik.common

import org.joda.time.DateTime
import scala.collection.mutable
import scala.annotation.tailrec

trait ValidityDates {
  def startDate: Option[DateTime]
  def endDate: Option[DateTime]
}

// TODO: Create activation and validity date range auditing
object ValidityDates {
  def earlier(dateA: Option[DateTime], dateB: Option[DateTime]): Option[DateTime] = {
    (dateA, dateB) match {
      case (Some(a), Some(b)) => if (a.compareTo(b) <= 0) dateA else dateB
      case (Some(_), None) => dateA
      case _ => dateB
    }
  }

  def later(dateA: Option[DateTime], dateB: Option[DateTime]): Option[DateTime] = {
    (dateA, dateB) match {
      case (Some(a), Some(b)) => if (a.compareTo(b) >= 0) dateA else dateB
      case (Some(_), None) => dateA
      case _ => dateB
    }
  }

  /** Extracts all the validity date range boundaries if and only if the dates create a contiguous period of time.  Otherwise it returns an empty sequence */
  def getContiguousDateRanges(d: Seq[ValidityDates]): Seq[DateTime] = {
    val sortedByStartDate = d.sortWith((a, b) => earlier(a.startDate, b.startDate) == a.startDate)

    val beginning = new mutable.ListBuffer[DateTime]
    
    for {
      head <- sortedByStartDate.headOption
      start <- head.startDate
    } { beginning += start }

    sortedByStartDate.foldLeft(beginning)((dates, validityDate) => {
      val resultOption = for {
        lastEnd <- dates.lastOption
        start <- validityDate.startDate
        end <- validityDate.endDate if start.compareTo(end) <= 0
      } yield {
        if (lastEnd == start) {
          if (start == end) dates
          else dates += end
        } else new mutable.ListBuffer[DateTime]
      }
      resultOption.getOrElse(new mutable.ListBuffer[DateTime])
    })
  }
}