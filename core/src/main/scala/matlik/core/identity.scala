package matlik.core

import matlik.common._

/** 
 * A PK should be implemented as a type specific case class containing a simple type (long, string) as its value.
 */ 
trait PK

trait PKLong extends PK { 
  def value: Long
}

trait PKString extends PK {
  def value: String
}

/**
 * An Identified object can be referenced by an ID of type Identifier
 */
trait Identified[T <: PK] {
  def id: Option[T]
}
