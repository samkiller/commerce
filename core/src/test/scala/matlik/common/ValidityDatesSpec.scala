package matlik.common

import java.util.Date
import org.scalatest.WordSpec
import org.scalatest.Matchers
import org.junit.runner.RunWith
import org.joda.time.DateTime


@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class ValidityDatesSpec extends WordSpec with Matchers {
  "ValidityDates" should {
    "compare/select earlier and later values appropriately" in {
      val a: Option[DateTime] = Option(DateTime.now())
      val b: Option[DateTime] = Option(DateTime.now().plusDays(1))
      
      // Earlier
      assertResult(None)(ValidityDates.earlier(None, None))
      assertResult(a)(ValidityDates.earlier(a, None))
      assertResult(a)(ValidityDates.earlier(None, a))
      assertResult(a)(ValidityDates.earlier(a, b))
      assertResult(a)(ValidityDates.earlier(b, a))
      
      // Later
      assertResult(None)(ValidityDates.later(None, None))
      assertResult(a)(ValidityDates.later(a, None))
      assertResult(a)(ValidityDates.later(None, a))
      assertResult(b)(ValidityDates.later(a, b))
      assertResult(b)(ValidityDates.later(b, a))
    }
    
    "find contiguous date ranges" in {
      case class Dates(startDate: Option[DateTime], endDate: Option[DateTime]) extends ValidityDates
      val now = DateTime.now()
      
      val a = Dates(Option(now), Option(now.plusDays(1)))
      val b = Dates(Option(now.plusDays(1)), Option(now.plusDays(2)))
      val c = Dates(Option(now.plusDays(2)), Option(now.plusDays(3)))
      
      assertResult(Seq(now, now.plusDays(1), now.plusDays(2), now.plusDays(3)))(ValidityDates.getContiguousDateRanges(Seq(a,b,c)))
      assertResult(Seq())(ValidityDates.getContiguousDateRanges(Seq(a,c)))
      assertResult(Seq())(ValidityDates.getContiguousDateRanges(Seq(a.copy(endDate = None),b,c)))
      assertResult(Seq())(ValidityDates.getContiguousDateRanges(Seq(a,b.copy(startDate = None), c)))
      assertResult(Seq())(ValidityDates.getContiguousDateRanges(Seq(a,b.copy(startDate = b.endDate, endDate = b.startDate), c)))
    }
  }

}