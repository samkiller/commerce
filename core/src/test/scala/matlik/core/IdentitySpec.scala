package matlik.core
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class IdentitySpec extends WordSpec with Matchers {
  // This test class doesn't really validate via audits, but instead confirms the type definitions work by successfully compiling.

  "Identity classes" should {
    "successfully compile" in {

      trait MyID extends PK
      case class MyIDImpl() extends MyID
      case class MyObj(id: Option[MyIDImpl]) extends Identified[MyIDImpl]

      // This is an example of one giving me trouble
      {
        import matlik.commerce.pk.EntryPK

        case class EntryPriceID(value: String) extends PKString {
          def this() = this(java.util.UUID.randomUUID.toString)
        }
        
        trait EntryPrice extends Identified[EntryPriceID] {
          def entry: EntryPK

          def updateEntryPrice(
            id: Option[EntryPriceID] = id,
            entry: EntryPK = entry)
        }
      }
    }
  }
}