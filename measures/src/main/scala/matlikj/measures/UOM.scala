package matlik.measures

import scala.math.BigDecimal
import scala.math.BigDecimal.RoundingMode._

trait UOM {
  def longLabel: String;
  def shortLabel: String;
  def symbol: String;
  def significantDecimalDigits: Int;
  def minimumValue: BigDecimal;
  def defaultRoundingMode = UNNECESSARY
  override def toString() = shortLabel
}

trait Quantafiable[T <: UOM]  extends Ordered[Quantafiable[T]] {
  type selfType <: Quantafiable[T]
  private val ZERO: BigDecimal = 0
  def qty: BigDecimal
  def uom: T
  def +(that: selfType)(implicit converter: Converter[selfType, T]): Quantafiable[T]
  def +(that: BigDecimal): Quantafiable[T]
  def -(that: selfType)(implicit converter: Converter[selfType, T]): Quantafiable[T]
  def -(that: BigDecimal): Quantafiable[T]
  def *(that: BigDecimal): Quantafiable[T]
  def /(that: BigDecimal): Quantafiable[T]
  def round(
      roundingMode: RoundingMode = uom.defaultRoundingMode, 
      minimumValue: BigDecimal = uom.minimumValue, 
      precision: Int = uom.significantDecimalDigits ): Quantafiable[T]
  
  def compare(that: Quantafiable[T]) = {
    assert(this.uom == that.uom, s"Attempted comparison between '${this.uom}' and '${that.uom}, but UOM must be the same.")
    qty compare that.qty
  }

  def isZero = qty == ZERO
  def isPositive = qty > ZERO
  def isNegative = qty < ZERO
}

trait Converter[A <: Quantafiable[B], B <: UOM] {
  /* Change the qty's UOM from one UOM subtype to another */
  def convert(qty: A, uom: B): A
  def findConversionRate(from: B, to: B): Option[BigDecimal]
}

