package matlik.measures

import scala.math.BigDecimal
import scala.math.BigDecimal.RoundingMode._

trait Currency extends UOM

object Money {
  val ZERO = BigDecimal(0)
  val HALF = BigDecimal(0.5)
  val NEG_HALF = BigDecimal(-0.5)
  
//  implicit object strictOrderByQty extends Ordering[Money] {
//    def compare(a: Money, b: Money) = {
//      val converted = if (a.uom == b.uom) b else SimpleImplicits.SimpleConverter.convert(b, a.uom)
//      a.qty compare b.qty
//    }
//  }
}

case class Money(val value: BigDecimal, val cur: Currency) extends Quantafiable[Currency] {
  override type selfType = Money
  override def uom = cur
  override def qty = value

  override def +(that: Money)(implicit converter: Converter[Money, Currency]): Money = {
    val converted = if (this.uom == that.uom) that else converter.convert(that, uom)
    this.copy(value = value + converted.qty)
  }

  override def +(that: BigDecimal): Money = this.copy(value = value + that)

  override def -(that: Money)(implicit converter: Converter[Money, Currency]): Money = {
    val converted = if (this.uom == that.uom) that else converter.convert(that, uom)
    this.copy(value = value - converted.qty)
  }

  override def -(that: BigDecimal): Money = this.copy(value = value - that)

  override def *(that: BigDecimal): Money = this.copy(value = value * that)

  override def /(that: BigDecimal): Money = this.copy(value = value / that)

  override def round(roundingMode: RoundingMode = uom.defaultRoundingMode, minimumValue: BigDecimal = uom.minimumValue, precision: Int = uom.significantDecimalDigits): Money = {
    import Money._
    val remainder = value % minimumValue
    if (remainder == ZERO) return this

    val base = value - remainder
    
    val adjustment: BigDecimal = roundingMode match {
      // Larger number, including more negative
      case UP =>
        if (value > ZERO) minimumValue
        else if (value < ZERO) -minimumValue
        else ZERO

      // Smaller number, including less negative
      case DOWN => ZERO

      // Less positive
      case FLOOR => if (base > ZERO) ZERO else -minimumValue

      // More positive
      case CEILING => if (base > ZERO) minimumValue else ZERO

      // Half or more, larger number including more negative
      case HALF_UP =>
        val ratio = remainder / minimumValue
        if (ratio >= HALF) minimumValue
        else if (ratio <= NEG_HALF) -minimumValue
        else ZERO

      // half or less, smaller number including less negative
      case HALF_DOWN =>
        val ratio = remainder / minimumValue
        if (ratio > HALF) minimumValue
        else if (ratio < NEG_HALF) -minimumValue
        else ZERO

      // If exactly half, round to nearest even number
      case HALF_EVEN => ???

      // No rounding?  If we have a remainder, it is required.
      case UNNECESSARY => throw new ArithmeticException("Rounding necessary")

    }

    this.copy(value = (base + adjustment).setScale(uom.significantDecimalDigits))
  }
}

object SimpleImplicits {

  object USD extends Currency {
    val longLabel = "US Dollars"
    val shortLabel = "USD"
    val symbol = "$"
    val significantDecimalDigits = 2
    val minimumValue = BigDecimal("0.01")
    override val defaultRoundingMode = HALF_UP
  }

  object USD_Dollars extends Currency {
    val longLabel = "US Dollars"
    val shortLabel = "USD"
    val symbol = "$"
    val significantDecimalDigits = 0
    val minimumValue = BigDecimal("1")
    override val defaultRoundingMode = DOWN
  }

  implicit object SimpleConverter extends Converter[Money, Currency] {
    override def convert(qty: Money, uom: Currency): Money = {
      if (qty.uom == uom) return qty
      val converted = findConversionRate(qty.uom, uom).map(rate => Money(qty.qty * rate, uom))
      converted getOrElse sys.error("No conversion found from " + qty.uom + " to " + uom)
    }

    override def findConversionRate(from: Currency, to: Currency): Option[BigDecimal] = {
      val lookup = Map{ ("USD", "USD") -> BigDecimal("1") }
      lookup.get((from.shortLabel, to.shortLabel))
    }
      
  }
}
