# Performance

The core price calculation framework has been implemented as a synchronous, single threaded, CPU bound process. All potentially blocking operations have been left open for users of the library to implement. This approach was taken for multiple reasons:

1. Eliminate convoluted async callbacks to simplify core functionality, making it easier to understand and evolve.
2. Let user requirements drive interfacing with persistence (relational vs. nosql, web service calls, blocking vs. non-blocking, single vs. batch) 

Fortunately, this does not prevent the core from being used in an asynchronous and nonblocking way. At first, this requirement may suggest the use of Futures or Scala's async/await constructs (http://docs.scala-lang.org/sips/pending/async.html).  However, this kind of asynchronous processing only becomes beneficial if multiple potentially blocking IO functions can be kicked off in an ahead-of-time fashion. Since the core functionality has a bias toward synchronous processing, it only asks for data as needed in a direct style. Both Futures and the async/await approaches, if implemented at the data interfacing points, would kick off the appropriate query in an async thread, but immediately await for the response. While this may be beneficial for complex data accesses that can aggregate many async calls into a single await, the common case would actually be worse than the straightforward synchronous processing because multiple threads are needed to get the same work done in essentially the same amount of time.

In practice, the core will need to be "wrapped" in some way to take advantage of more advanced nonblocking API. One such approach could leverage Akka actors.

## Nonblocking Async with Akka Actors

Actors are a concurrency abstraction that are intended to decouple your logic from the more expensive JVM thread, allowing thousands to millions of Actors run on a very small number of threads. This has the potential of increasing throughput by more efficiently reusing thread pool resources. Wrapping the pricer in an actor and each of the blocking IO within actors (limit the number of open requests) or Akka futures, then ask pattern (http://doc.akka.io/docs/akka/snapshot/scala/actors.html) can be used to asynchronously process IO (ideally async, but also blocking) using Akka's thread management effectively. 

The actor context would need to be pulled into the CalculationContext to relay important Akka information into the interfacing endpoints to be implemented by the user.

## Parallelism

Parallelism within the core is not supported, at least not yet. It is not uncommon for single threaded solutions to outperform parallel processing due to the context switching overhead between threads, which is particularly the case when the parallelization is applied to smaller data sets. Putting this into the core at this time has been deemed too early, and therefore evil. The appropriate micro-benchmarking is required before any such changes are entertained.

The core is designed to be fully immutable and thread safe, so even though it is synchronous in nature, multiple price calculations can be executed simultaneously.

Parallelism for a single price calculation (if latency must be reduced) can be achieved within the user provided data interfacing points. Take the before mentioned ahead-of-time blocking IO as an example.  A calculation run scoped cache of price rows and grouper data could be instantiated and bound to the CalculationContext upon starting the price calculation.  Then, in parallel, all anticipated values could be queried and populated into the cache, possibly before the calculation requests the data. While this can reduce the latency for a single price run, it will likely increase the overhead (CPU and memory) of each price run because extra data may be loaded and never used due to CalculationStep stacking rules.

## Caching

There are several places caching can be applied:
1. CalculationValueSequenceLoader - Load once for many calculations.
2. PriceRowSelector
3. Step and Entry Restrictors - If data driven
4. Grouper - If data driven
