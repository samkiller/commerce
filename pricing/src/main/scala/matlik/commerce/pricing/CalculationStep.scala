package matlik.commerce.pricing

import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.common.Message
import matlik.common.ValidityDates
import matlik.measures._
import org.scalactic.ConversionCheckedTripleEquals._
import scala.annotation.tailrec
import scala.concurrent.Future
import matlik.commerce.pk.CalculationStepPK

object CalculationStep {

  def commonContextEnrichment(id: CalculationStepPK, context: CalculationContext): CalculationContext = {
    context.copyContext(
      calculationStep = Option(id),
      calculationDepth = context.calculationDepth.map(_ + 1).orElse(Option(1)))
  }
  
  def filterEntriesByRestrictions(view: EntryPriceSeq, restrictions: Seq[EntryPriceRestriction]): EntryPriceSeq = {
    var filtered = view
    for (restriction <- restrictions) {
      restriction match {
        case r: EntryPriceDenied => filtered = filtered.filter { e => (e.id !== None) && (e.id.get !== r.entryPrice) }
        case _ => // Do nothing
      }
    }
    filtered
  }

  /**
   * Filters the sequence of EntryPrices, returning a tuple of sequences.  The tuple returned is in the order of (consumed, available) where
   * "consumed" means the newly created EntryPrice objects with new IDs and the "available" are EntryPrice objects whose qty may have been adjusted
   * but have IDs from existing entries.
   */
  def filterEntriesByCalculationResults(view: EntryPriceSeq, results: Seq[CalculationStepResult]): (EntryPriceSeq, EntryPriceSeq) = {
    var available: EntryPriceSeq = view
    var consumed: EntryPriceSeq = Seq.empty[EntryPrice]
    for (result <- results) {
      result match {

        case apsr: AggregatePriceStepResults => {
          val (c, a) = filterEntriesByCalculationResults(available, apsr.childResults)
          consumed = consumed ++ c
          available = a
        }

        case epm: EntryPriceMutation if epm.fromEntryPrice.length > 0 => {
          /*
           * This moves qty in "available" to "toMutate" and leaves untouched in "remaining".  To support merge calculation values, it is
           * possible the fromEntryPrice won't be found in the list of remaining (pulled from multiple calculation values).  This could be
           * fixed by having those merges concatenate the entry prices (seemingly double counting) before using this logic to merge them into one.
           */
          val (toMutate, remaining) = epm.fromEntryPrice.foldLeft((Seq[EntryPrice](), available)) { (prevResults, fromEntryPrice) =>
            val (_mutating, _remaining) = prevResults
            val (nextMutate, nextRemaining) = _remaining.partition(p => p.id.map(_ === fromEntryPrice).getOrElse(false))
            require(nextMutate.length === 1,
              s"""|Unexpected number of EntryPrice objects found for mutation.  Should be 1, but was ${nextMutate.length}
                |  EntryPriceID: ${fromEntryPrice} --- nextMutate: ${nextMutate} --- _remaining ${_remaining} --- mutation ${epm}""".stripMargin)
            (nextMutate ++ _mutating, nextRemaining)
          }
          val (modified, unmodified) = mutateEntryPrice(toMutate, epm)
          assert((modified, unmodified) !== (None, Seq()), "The mutateEntryPrice should never return None for both the unmodified and modified values")
          available = unmodified ++ remaining
          modified.foreach { m => consumed = consumed :+ m }
        }

        case _ => // All others will not consume quantity

      }
    }
    (consumed, available)
  }

  /**
   * Splits the fully applying groups from those not fully applying.  When they do not fully apply, all contained ApplicationResult
   * instances have their applies flag set to false.
   */
  def filterFullyApplyingGroups(groups: Seq[ApplicationResultGroup]): (Seq[ApplicationResultGroup], Seq[ApplicationResultGroup]) = {
    var applies: Seq[ApplicationResultGroup] = Seq.empty
    var doesNotApply: Seq[ApplicationResultGroup] = Seq.empty

    for (group <- groups) {
      if (group.exists(result => !result.applies)) {
        doesNotApply = doesNotApply :+ normalizeAppliesFlag(group)
      } else applies = applies :+ group
    }
    (doesNotApply, applies)
  }

  def normalizeAppliesFlag(group: ApplicationResultGroup): ApplicationResultGroup = {
    for (result <- group) yield {
      result match {
        case ea: EntryApplication =>
          if (ea.applies) ea else ea.copy(applies = false)

        case ca: CompoundApplication =>
          if (ca.applies) ca else ca.copy(applies = false)
      }
    }
  }

  /**
   *  Applies the given mutation to the EntryPrice. Since a mutation can be partial, complete, or not at all, the results must be optional.
   *  The results are returned as a tuple of (modified, unmodified) where "unmodified" means the EntryPrice may be reduced in qty, but the ID
   *  does not change.
   */
  def mutateEntryPrice(entryPrices: Seq[EntryPrice], mutation: EntryPriceMutation): (Option[EntryPrice], Seq[EntryPrice]) = {
    assert(entryPrices.size === mutation.fromEntryPrice.size && entryPrices.forall(entryPrice => mutation.fromEntryPrice.contains(entryPrice.id.get)),
      s"Mutation cannot be applied to unmatched ID(s).  Expected entries in ${mutation.fromEntryPrice}, but got ${entryPrices.map(_.id)}")

    val consumedQty = mutation match {
      case q: QuantafiableResult => q.quantity
      case _ => {
        val qty = entryPrices.head.quantity
        assert(entryPrices.forall(_.quantity === qty), s"Invalid mutation of multiple enteries of different quantity into one: ${entryPrices}")
        qty
      }
    }

    mutation match {
      case a: EntryApplication => {
        if (!a.applies) {
          (None, entryPrices)
        } else {
          var startDate = a.startDate
          var endDate = a.endDate
          
          val unmodified = for { entryPrice <- entryPrices } yield { 
            startDate = ValidityDates.later(entryPrice.startDate, startDate)
            endDate = ValidityDates.earlier(entryPrice.endDate, endDate)

            // If the quantity is partially consumed, reduce the quantity on the original "from", otherwise drop it
            val remainingQty = entryPrice.quantity - consumedQty
            if (remainingQty.isPositive) {
              Option(entryPrice.updateEntryPrice(quantity = remainingQty))
            } else None
          }
          
          val modified = Option(entryPrices.head.updateEntryPrice(
              id = Option(a.toEntryPrice),
              quantity = consumedQty,
              startDate = startDate,
              endDate = endDate,
              unitPrice = a.unitPrice))
              
          (modified, unmodified.flatten)
        }
      }
    }
  }

  abstract class AbstractCalculationStep extends CalculationStep with CalculationStepRestrictor with EntryPriceRestrictor with PriceRowSelector with Grouper with ResultCalculator {
    /** Update the calculation step specific details within the context */
    def enrichContext(context: CalculationContext): CalculationContext = CalculationStep.commonContextEnrichment(id, context)
    
    /** Allow the step by default */
    def getStepRestriction(context: CalculationContext): StepRestriction = CalculationStepRestrictor.allow(context)

    /** No entry restrictions by default */
    def getEntryPriceRestrictions(context: CalculationContext): Seq[EntryPriceRestriction] = Seq.empty

    /** Given a Seq of EntryPrice objects, remove those impacted by the seq of restrictions **/
    def filterEntriesByRestrictions(view: EntryPriceSeq, restrictions: Seq[EntryPriceRestriction]): EntryPriceSeq =
      CalculationStep.filterEntriesByRestrictions(view: EntryPriceSeq, restrictions: Seq[EntryPriceRestriction])

    /** Given a Seq of EntryPrice objects, remove those consumed by the CalculationResults **/
    def filterEntriesByCalculationResults(view: EntryPriceSeq, results: Seq[CalculationStepResult]): (EntryPriceSeq, EntryPriceSeq) =
      CalculationStep.filterEntriesByCalculationResults(view: EntryPriceSeq, results: Seq[CalculationStepResult])

    /** Calculate results (all results, including fully applied prices) for the view */
    def calculate(contextIn: CalculationContext)(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult] = {
      val context = enrichContext(contextIn)

      // First see if the CalculationStep should do anything
      val stepRestriction = getStepRestriction(context)

      val applicationResults: Seq[CalculationStepResult] = stepRestriction match {
        case x: StepAllowed => {
          // If so, filter entries (inclusions and restrictions)
          val entryRestrictions = getEntryPriceRestrictions(context)
          val filteredEntries = filterEntriesByRestrictions(context.stepView, entryRestrictions)

          // If entries left to process, continue with calculation
          val allCalculatedResults: Seq[CalculationStepResult] =
            if (filteredEntries.isEmpty) {
              Seq.empty
            } else {
              val filteredView = context.copyContext(stepView = filteredEntries)

              // If entries left to process, obtain a grouping for price application
              val groups = getApplicationGroups(filteredView)

              val (incompleteGroups, applyingGroups) = filterFullyApplyingGroups(groups)

              // Select a price row for each group
              val priceRows = selectPriceRows(filteredView, applyingGroups)

              // Calculate complete results
              val calculatedResults = calculateResults(filteredView, applyingGroups, priceRows)

              // Recursively call calculate excluding consumed quantity
              val (modifiedEntries, remainingEntries) = filterEntriesByCalculationResults(filteredView.stepView, calculatedResults)
              val subsequentResults: Seq[CalculationStepResult] = if (!remainingEntries.isEmpty && !modifiedEntries.isEmpty) {
                val remainingView = context.copyContext(stepView = remainingEntries)
                calculate(remainingView)
              } else Seq.empty

              // Aggregate calculation results
              val aggregateCalculatedResults = if (subsequentResults.isEmpty) calculatedResults else calculatedResults ++ subsequentResults
              incompleteGroups.flatten ++ aggregateCalculatedResults
            }

          // Return all determined results
          entryRestrictions ++ allCalculatedResults
        }

        case x: StepDenied => Seq.empty
      }

      stepRestriction +: applicationResults
    }
  }

  class ComposibleCalculationStep(
    stepID: CalculationStepPK,
    grouper: Grouper,
    priceRowSelector: PriceRowSelector,
    resultCalculator: ResultCalculator,
    stepRestrictor: Option[CalculationStepRestrictor],
    entryRestrictor: Option[EntryPriceRestrictor]) extends AbstractCalculationStep {

    override def id: CalculationStepPK = stepID

    val stepRestrictionF = stepRestrictor.map(_.getStepRestriction _).getOrElse(super.getStepRestriction _)
    override def getStepRestriction(context: CalculationContext) = stepRestrictionF(context)

    val entryRestrictionF = entryRestrictor.map(_.getEntryPriceRestrictions _).getOrElse(super.getEntryPriceRestrictions _)
    override def getEntryPriceRestrictions(context: CalculationContext) = entryRestrictionF(context)

    def getApplicationGroups(context: CalculationContext) = grouper.getApplicationGroups(context)

    def selectPriceRows(context: CalculationContext, groups: Seq[ApplicationResultGroup]) =
      priceRowSelector.selectPriceRows(context, groups)

    def calculateResults(context: CalculationContext, groups: Seq[ApplicationResultGroup], priceRows: Seq[PriceRow])(implicit converter: Converter[Money, Currency]) =
      resultCalculator.calculateResults(context, groups, priceRows)
  }
}

trait CalculationStep {
  def id: CalculationStepPK

  /** Needed to make context enrichment available outside the CalculationStep for use in the Price Calculator (Note: I don't really like this...) */
  def enrichContext(context: CalculationContext): CalculationContext
  
  /** May return zero or more CalculationResult objects that may or may not impact the end price */
  def calculate(context: CalculationContext)(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult]
}
