package matlik.commerce.pricing

import matlik.core.Identified
import matlik.core.PKLong
import matlik.core.PKString
import matlik.measures.Money
import matlik.commerce.Quantity
import matlik.common.ValidityDates
import matlik.commerce.pk._
import org.joda.time.DateTime

/**
 * Transactional pricing information corresponding at the potentially sub-entry level.  The EntryPrice itself is not 
 * generally persisted.  Rather, the CalculationResults for the SalesDocument are persisted, and the EntryPrice objects 
 * are re-generated using 
 */
trait EntryPrice extends Identified[EntryPriceID] with ValidityDates {
  def entry: EntryPK
  def quantity: Quantity
  def unitPrice: Option[Money]
  
  // The mutation of the EntryPrice object in an immutable way.  Case classes are clean, but not extensible.
  def updateEntryPrice(
    id: Option[EntryPriceID] = id,
    startDate: Option[DateTime] = startDate,
    endDate: Option[DateTime] = endDate,
    entry: EntryPK = entry,
    quantity: Quantity = quantity,
    unitPrice: Option[Money] = unitPrice): EntryPrice
}

//object EntryPrice {
//  def sum(a: EntryPrice, b:EntryPrice): EntryPrice = {
//    assert(a.entry == b.entry, s"Cannot sum two EntryPrice instances with different quantity: sum($a, $b)")
//    assert(a.quantity == b.quantity, s"Cannot sum two EntryPrice instances with different quantity: sum($a, $b)")
//    val startDate = ValidityDates.later(a.startDate, b.startDate)
//    val endDate = ValidityDates.earlier(a.endDate, b.endDate)
//    
//    val unitPrice = for {
//      aPrice <- a.unitPrice
//      bPrice <- b.unitPrice } yield aPrice + bPrice
//    
//    a.updateEntryPrice(
//        id = Option(new EntryPriceID()), 
//        startDate = startDate, 
//        endDate = endDate, 
//        unitPrice = unitPrice)
//  }
//  
//}

case class EntryPriceID(value: String) extends EntryPricePK with PKString {
  def this() = this(java.util.UUID.randomUUID.toString)
}

