package matlik.commerce.pricing

import matlik.common.Message

/** Reduces the set of potentially applicable entries for this price step */
trait EntryPriceRestrictor {
  def getEntryPriceRestrictions(context: CalculationContext): Seq[EntryPriceRestriction]
}

object EntryPriceRestrictor {
  def allow(context: CalculationContext, e: EntryPrice, messages: Seq[Message] = Seq.empty) = EntryPriceAllowed(
    step = context.calculationStep,
    depth = context.calculationDepth,
    entry = e.entry,
    entryPrice = e.id.get,
    messages = messages)

  def deny(context: CalculationContext, e: EntryPrice, messages: Seq[Message] = Seq.empty) = EntryPriceDenied(
    step = context.calculationStep,
    depth = context.calculationDepth,
    entry = e.entry,
    entryPrice = e.id.get,
    messages = messages)
}
