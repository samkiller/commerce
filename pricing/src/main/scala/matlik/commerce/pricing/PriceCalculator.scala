package matlik.commerce.pricing

import matlik.commerce.pk.EntryPK
import matlik.commerce.pk.ProductPK
import matlik.commerce.pk.SalesDocumentPK
import matlik.common.Message
import matlik.common.Messages
import matlik.common.ValidityDates
import matlik.measures.Converter
import matlik.measures.Currency
import matlik.measures.Money
import matlik.measures.UOM
import org.joda.time.DateTime
import scala.annotation.tailrec
import scala.concurrent.Future
import scala.concurrent.ExecutionContext

object PriceCalculator {
  implicit class PriceDocumentOpts(val doc: PriceDocument) extends AnyVal {
    def getValue(cvName: CalculationValueName): Option[CalculationValue] = doc.calculationValues.find(_.name == cvName)
  }

  implicit class CalculationValueOpts(val cv: CalculationValue) extends AnyVal {
    def getPrices(entry: EntryPK): Seq[EntryPrice] = cv.prices.filter(_.entry == entry)

    def getResults(entry: EntryPK): Seq[CalculationResult] = {
      def getResults(result: CalculationResult): Seq[CalculationStepResult] = {
        result match {
          case ca: CompoundApplication => {
            val relevantChildren = ca.childResults.flatMap(getResults(_))
            if (!relevantChildren.isEmpty) Seq(ca.copy(childResults = relevantChildren)) else Seq.empty
          }
          case e: CalculationStepResult with EntryLevel if e.entry == entry => Seq(e)
          case _ => Seq.empty
        }
      }
      cv.results.map(getResults(_)).flatten
    }

    def getMessages(entry: EntryPK): Seq[Message] = {
      def getMessages(result: CalculationResult): Seq[Message] = {
        result match {
          case ca: CompoundApplication => ca.childResults.foldLeft(ca.messages)((agg, next) => agg ++ next.messages)
          case m: Messages => m.messages
          case _ => Seq.empty
        }
      }
      getResults(entry).flatMap(getMessages(_))
    }
    
//    /** Gets a total price for the whole context sum of (qty * unitPrice) sliced by date range */
//    def getTotals(implicit converter: Converter[Money, Currency]): Seq[Option[TotalPrice]] = {
//      val groupsByDate = cv.prices.groupBy(p => (p.startDate, p.endDate))
//      val currencyOption = for {
//        price <- cv.prices.headOption
//        money <- price.unitPrice
//      } yield {
//        money.uom
//      }
//
//      for {
//        currency <- currencyOption.toSeq
//        (_, entries) <- groupsByDate
//      } yield {
//        val qtyPrices = for {
//          entry <- entries
//          unitPrice <- entry.unitPrice
//        } yield {
//          unitPrice * entry.quantity.qty
//        }
//        qtyPrices.reduce((a, b) => a + b)
//      }
//      ???
//    }
  }

  case class CalculationValueStep(name: CalculationValueName, step: CalculationStep)

  def extractEntryPrices(view: Seq[EntryPrice], results: Seq[CalculationStepResult]): Seq[EntryPrice] = {
    val filteredResults = CalculationStep.filterEntriesByCalculationResults(view, results)
    filteredResults._1 ++: filteredResults._2
  }

  /**
   * Load the top-level CalculationSteps relevant for the current context.  This method is meant to decide if a full or partial calculation should be performed
   * for the context.  By default, the last CalculationValue's results are used as the stepView for the next CalculationValue.  If this is not desirable, the
   * loaded CalculationStep must be changed to override the stepView passed in.
   */
  trait CalculationValueSequenceLoader {
    def loadCalculationValueSequence(context: CalculationContext): Seq[CalculationValueStep]
  }

  trait DefaultPriceDocumentLoader {
    def loadDefaultPriceDocument(context: CalculationContext): PriceDocument
  }

  abstract class AbstractPriceCalculator extends PriceCalculator with CalculationValueSequenceLoader with DefaultPriceDocumentLoader {
    def extractEntryPrices(view: Seq[EntryPrice], results: Seq[CalculationStepResult]): Seq[EntryPrice] = PriceCalculator.extractEntryPrices(view, results)

    @tailrec
    private def calculateValues(context: CalculationContext, valueCalcSeq: Seq[CalculationValueStep])(implicit converter: Converter[Money, Currency]): PriceDocument = {
      if (valueCalcSeq.isEmpty) context.priceDocument.get
      else {
        val cvStep = valueCalcSeq.head
        val step = cvStep.step
        val name = cvStep.name
        val results = step.calculate(context)
        val stepView = step.enrichContext(context).stepView
        val prices = extractEntryPrices(stepView, results)
        val calculationValue = CalculationValue(name, results, prices)
        val priceDocument = for {
          doc <- context.priceDocument
        } yield {
          doc.copyPriceDocument(calculationValues = calculationValue +: doc.calculationValues.filter(_.name != name))
        }
        val ctx = context.copyContext(priceDocument = priceDocument, stepView = calculationValue.prices)
        calculateValues(ctx, valueCalcSeq.tail)
      }
    }

    def calculate(context: CalculationContext)(implicit converter: Converter[Money, Currency]): PriceDocument = {
      // Make sure the PriceDocument is properly initialized
      val ctx = if (context.priceDocument == None) {
        context.copyContext(priceDocument = Option(loadDefaultPriceDocument(context)))
      } else context

      // Load the CalculationValueSequence
      calculateValues(ctx, loadCalculationValueSequence(context))
    }
  }

  class ComposiblePriceCalculator(
    calculationValueSequenceLoader: CalculationValueSequenceLoader,
    defaultPriceDocumentLoader: DefaultPriceDocumentLoader) extends AbstractPriceCalculator {
    def loadCalculationValueSequence(context: CalculationContext): Seq[CalculationValueStep] = calculationValueSequenceLoader.loadCalculationValueSequence(context)
    def loadDefaultPriceDocument(context: CalculationContext): PriceDocument = defaultPriceDocumentLoader.loadDefaultPriceDocument(context)
  }
}

trait PriceCalculator {
  def calculate(context: CalculationContext)(implicit converter: Converter[Money, Currency]): PriceDocument
}
