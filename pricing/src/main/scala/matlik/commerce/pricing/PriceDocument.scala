package matlik.commerce.pricing

import matlik.core.PKLong
import matlik.commerce.pk._
import org.joda.time.DateTime

/**
 * A PricingDocument records completed calculations for use in business processing as well as persistence.
 * 
 * @param priced the date used against validity date ranges when resolving prices in the calculations
 * 
 * @param calculationValues the values that have been calculated.  Note that this collection
 * may be a subset of all calculations needed to be "fully priced".  Ideally, these values can be reused
 * during recalculation cycles to reduce the I/O and processing needed to become fully priced.
 */
trait PriceDocument {
  def id: Option[PriceDocumentID]
  //def created: DateTime
  //def modified: DateTime
  def priced: DateTime
  def calculationValues: Seq[CalculationValue]
  
  def copyPriceDocument(
    id: Option[PriceDocumentID] = id,
    priced: DateTime = priced,
    calculationValues: Seq[CalculationValue] = calculationValues
  ): PriceDocument
}

case class PriceDocumentID(value: Long) extends PKLong
