package matlik.commerce.pricing

import matlik.common.ValidityDates
import matlik.core.{Identified, PKLong}
import matlik.measures.Money
import matlik.commerce.QUnit

case class PriceRowID(value: Long) extends PKLong

trait PriceRow extends Identified[PriceRowID] with ValidityDates {
  // Quantities used for selection
  def minQty: Option[Long]
  def maxQty: Option[Long]
  def unitOfMeasure: QUnit
  //def message: Option[String]
}

trait FixedPriceRow extends PriceRow {
  def price: Money // TODO: Change to just "price"
}

trait PercentPriceRow extends PriceRow {
  def percent: Double
}

/**
 * Complex prices are designed to support complex pricing scenarios that don't map to an evenly
 * distributed per Unit price. These kinds of prices should calculate with an entry unit of 1
 * but with varying sub quantities.  This can be used, for example, when selling a licensed product
 * that has a per-seat charge only after the first 5 seats. This type must be converted to a
 * UnitPriceRow for use within the core pricing service. 
 */
trait ComplexPriceRow extends PriceRow {
  // The starting base price (like a cover charge)
  def startPrice: Money
  
  // Sub-unit adjustments (e.g. 1 license used by 3 users.)
  def perUnitQtyPrice: Option[Money]
  def perUnitGiveAwayQty: Option[Long]
  def toPriceRow: PriceRow
}
