package matlik.commerce.pricing

import matlik.commerce.pricing._
import matlik.commerce.pricing.CalculationStep._
import matlik.common.Message
import org.scalactic.ConversionCheckedTripleEquals._
import scala.Option.option2Iterable
import scala.math.BigDecimal.int2bigDecimal

/**
 * Selects groups of entries to have results generated together.  Multiple groups may exist if more
 * than one kind of result is required (e.g. buy 3 get 2 free would have two views returned, the first
 * corresponding to the buy side, and the second corresponding to the get side).  Note that the price
 * step must have the exact same number of price rows and generators to map 1:1 to the resulting groups
 * of entries.
 *
 * Input: A CalculationView containing a filtered stepView collection of PriceEntries that are potentially
 *        applicable (all filtering complete)
 * Output: ApplicationResultGroups that may or may not (with could have fired up-sell messaging) apply
 */
trait Grouper {
  def getApplicationGroups(context: CalculationContext): Seq[ApplicationResultGroup]
}

/** 
 * Returns a single ApplicationResultGroup containing all entry prices in the stepView corresponding
 * to the EntryPK of the stepView's head element.  This grouper should be selected when EntryPrices
 * for a given EntryPK should be adjusted in a consistent way, but one at a time. This is similar
 * to the BathEntryGrouper, however the batch version should be preferred when recursion is not needed,
 * as the result structure will be shallower and simpler.
 */
object SingleEntryGrouper extends Grouper {
  override def getApplicationGroups(context: CalculationContext): Seq[ApplicationResultGroup] = {
    val entryPrices = context.stepView
    Seq(for {
      head <- entryPrices.headOption.toSeq
      entryPrice <- entryPrices if entryPrice.entry === head.entry
    } yield new EntryApplication(context, entryPrice))
  }
}

/** 
 * Group all stepView EntryPrices by their EntryPK. This grouper should be selected when all EntryPrices
 * for a given EntryPK should be adjusted in a consistent way.  This grouper will return groups for all
 * the EntryPrices on the stepView in a single call, so should be used when the calculation result can
 * be determined all at once.
 */
object BatchEntryGrouper extends Grouper {
  override def getApplicationGroups(context: CalculationContext): Seq[ApplicationResultGroup] = {
    for {
      (entryId, group) <- context.stepView.groupBy(_.entry).toSeq
    } yield {
      // Convert the group of EntryPrice objects into results
      for { entryPrice <- group } yield new EntryApplication(context, entryPrice)
    }
  }
}

/**
 * Return groups of applications with quantity of 1 for each EntryPK in the stepView.  This is generally
 * used in more complex price step combinations, such as BOGO and quantity bundles. When the optional
 * numOfEntries parameter is set, the specified number of entries must exist for the grouper to apply, and
 * only that quantity will be consumed. The default behavior is to apply to one of each entry in the stepView.
 */
case class OnePerEntryGrouper(numOfEntries: Option[Int] = None, successMessages: Seq[Message] = Seq.empty, failMessages: Seq[Message] = Seq.empty) extends Grouper {
  override def getApplicationGroups(context: CalculationContext): Seq[ApplicationResultGroup] = {
    val groupedByEntry = context.stepView.groupBy(_.entry).toSeq
    for {
      (entryId, group) <- groupedByEntry
    } yield {
      // Create one EntryApplication for numOfEntries, or all of None
      val count = numOfEntries.getOrElse(groupedByEntry.size)
      val applies = count <= groupedByEntry.size
      val messages = if (applies) successMessages else failMessages
      for { entryPrice <- group } yield {
        val optionOne = entryPrice.quantity.copy(value = 1)
        new EntryApplication(context, entryPrice, quantity = Option(optionOne), applies = applies, messages = messages)
      }
    }
  }
}

/** Create a group for each entry price in the stepView. */
object AllGrouper extends Grouper {
  override def getApplicationGroups(context: CalculationContext): Seq[ApplicationResultGroup] = {
    Seq(for {
      entryPrice <- context.stepView.toSeq
    } yield {
      new EntryApplication(context, entryPrice)
    })
  }
} 
