package matlik.commerce.pricing

import scala.collection.mutable
import matlik.commerce.pricing.PriceCalculator._
import matlik.commerce.pk.EntryPK
import matlik.commerce.pk.CalculationStepPK
import matlik.commerce.QUnit
import matlik.commerce.Quantity
import matlik.common.Message
import matlik.common.ValidityDates
import matlik.measures.Converter
import matlik.measures.Money
import matlik.measures.Currency
import org.scalactic.ConversionCheckedTripleEquals._
import org.scalactic.Requirements._
import scala.annotation.tailrec

trait MergeGrouper {
  /** Decide what EntryPrices should be merged together. The inner collection of EntryPrice will be merged. */
  def groupForMerge(context: CalculationContext): Seq[EntryPriceSeq]
}

trait MergeResultCalculator {
  /** Decide how the previously grouped EntryPrices should be merged into a result.  This may sum the unit price, or merge qty at the same price */
  def doMerge(context: CalculationContext, entryPrices: Seq[EntryPrice])(implicit converter: Converter[Money, Currency]): ApplicationResult
}

abstract class AbstractAggregationCalculationStep extends CalculationStep with MergeGrouper with MergeResultCalculator {
  /** Update the calculation step specific details within the context */
  def enrichContext(context: CalculationContext): CalculationContext = CalculationStep.commonContextEnrichment(id, context)

  def calculate(context: matlik.commerce.pricing.CalculationContext)(implicit converter: matlik.measures.Converter[matlik.measures.Money, matlik.measures.Currency]) = {
    val updatedContext = enrichContext(context)
    val chunkedEntryPrices = groupForMerge(updatedContext)
    for (chunk <- chunkedEntryPrices) yield doMerge(updatedContext, chunk)
  }
}

trait MergeCriteria {
  def groupByCriteria(context: CalculationContext, toGroup: Seq[EntryPriceSeq]): Seq[Seq[EntryPrice]]
}

object MergeCriteria {
  /** Extract the quantity values, throwing an exception if the unit of measure does not match. */
  def getQtyValues(uom: QUnit, entries: Seq[EntryPrice]): Seq[BigDecimal] = for {
    entry <- entries
  } yield {
    require(entry.quantity.unit === uom)
    entry.quantity.value
  }

  /**
   * Given two sequences of numbers, merge them to common boundaries.  For example, given the following sequences:
   *   val a = Seq(4,5,1)
   *   val b = Seq(3,4,1,2)
   *
   *   The merge would result in the following breakdown where the 'x' corresponds to the boundary:
   *      Qty: 1 2 3 4 5 6 7 8 9 10
   *        a: . . . x . . . . x x
   *        b: . . x . . . x x . x
   *   result: . . x x . . x x x x
   *
   *   val result = Seq(3,1,3,1,1,1)
   */
  @tailrec
  final def determineQtyBoundary(a: Seq[BigDecimal], b: Seq[BigDecimal], merged: Vector[BigDecimal]): Seq[BigDecimal] = {
    (a.headOption, b.headOption) match {
      case (None, None) => merged
      case x @ (Some(_), None) => sys.error(s"Got an unexpected end state when merging, as qty of each Seq should be consumed at the same time: ${x}")
      case x @ (None, Some(_)) => sys.error(s"Got an unexpected end state when merging, as qty of each Seq should be consumed at the same time: ${x}")
      case (Some(aval), Some(bval)) =>
        if (aval < bval) determineQtyBoundary(a.tail, (bval - aval) +: b.tail, merged :+ aval)
        else if (aval > bval) determineQtyBoundary((aval - bval) +: a.tail, b.tail, merged :+ bval)
        else determineQtyBoundary(a.tail, b.tail, merged :+ aval)
    }
  }

  /**
   * Make the EntryPrice list conform to the quantity boundaries provided.  The inbound EntryPrice instances
   * should either be taken as-is (match the qty) or be split to fit a smaller qty.  A given quantity should
   * never be greater than an EntryPrice or the remainder of a split EntryPrice
   */
  @tailrec
  final def expandByQty(entries: List[EntryPrice], quantityBoundaries: Seq[BigDecimal], result: Vector[EntryPrice]): Seq[EntryPrice] = {
    (entries.headOption, quantityBoundaries.headOption) match {
      case (None, None) => result
      case x @ (Some(_), None) => sys.error(s"The quantityBoundaries do not align with the list of entry price: ${x}")
      case x @ (None, Some(_)) => sys.error(s"The quantityBoundaries do not align with the list of entry price: ${x}")
      case (Some(entry), Some(qty)) => {
        require(entry.quantity.value >= qty, "Something must have gone wrong with calculating the quantity boundaries")
        if (entry.quantity.value > qty) {
          val reduced = entry.updateEntryPrice(quantity = Quantity(qty, entry.quantity.uom))
          val remaining = entry.updateEntryPrice(quantity = entry.quantity - qty)
          expandByQty(remaining :: entries.tail, quantityBoundaries.tail, result :+ reduced)
        } else expandByQty(entries.tail, quantityBoundaries.tail, result :+ entry)
      }
    }
  }

  def toSeqCrossSections(sequences: Seq[Seq[EntryPrice]]) = {
    var crossSections = Vector.empty[Seq[EntryPrice]]
    var tails = sequences.toList

    def done = tails.isEmpty || tails(0).isEmpty

    while (!done) {
      var heads: List[EntryPrice] = Nil
      val toProcess = tails
      tails = Nil
      toProcess.foreach { seq =>
        heads = seq.head :: heads
        tails = seq.tail :: tails
      }
      heads = heads.reverse
      tails = tails.reverse
      crossSections = crossSections :+ heads
    }
    crossSections
  }
}

trait EntryPriceOrdering extends Ordering[EntryPrice]

case object GreaterUnitPriceOrdering extends EntryPriceOrdering {
  def compare(a: EntryPrice, b: EntryPrice) = {
    require(a.unitPrice.isDefined && b.unitPrice.isDefined)
    b.unitPrice.get compare a.unitPrice.get
  }
}

case object UnitPriceOrdering extends EntryPriceOrdering {
  def compare(a: EntryPrice, b: EntryPrice) = {
    require(a.unitPrice.isDefined && b.unitPrice.isDefined)
    a.unitPrice.get compare b.unitPrice.get
  }
}

case object QuantityOrdering extends EntryPriceOrdering {
  def compare(a: EntryPrice, b: EntryPrice) = {
    a.quantity compare b.quantity
  }
}

case class MergeCriteriaByRealtiveEntryPriceOrdering(orderer: EntryPriceOrdering) extends MergeCriteria {
  type PricesByEntry = Map[EntryPK, Seq[EntryPrice]]

  def groupByCriteria(context: CalculationContext, toGroup: Seq[EntryPriceSeq]) = {
    import MergeCriteria._

    require(toGroup.find(_.isEmpty) == None, s"Cannot merge an empty collection: ${toGroup}")

    // Take each EntryPriceSeq that spans all entries on a price document and chunk them into entry specific sub groups
    val consolidatedEntryGroupings = mutable.Map.empty[EntryPK, Seq[EntryPriceSeq]].withDefaultValue(Seq.empty)
    var entries = Set.empty[EntryPK]
    for { priceSeq <- toGroup } {
      val grouped = priceSeq.groupBy(_.entry)

      // Validate common list of entries
      if (entries.isEmpty) {
        entries = grouped.keySet
      } else {
        val current = grouped.keySet
        require(entries.size == current.size && entries.forall(current contains _), s"Cannot merge lists with misaligned entries: ${toGroup}")
      }

      for { (entry, priceSeqForEntry) <- grouped } {
        consolidatedEntryGroupings += (entry -> (priceSeqForEntry.sorted(orderer) +: consolidatedEntryGroupings(entry)))
      }
    }

    val groupedByEntry = for {
      (entry, groups) <- consolidatedEntryGroupings
    } yield {
      // March through the qty step by step to calculate the groupings in the context of a single entry.
      require(!groups.isEmpty && !groups.head.isEmpty)
      def expectedUOM = groups.head.head.quantity.uom

      // Determine common quantity boundaries to be merged in sort priority order
      val quantityBoundaries = groups.map(getQtyValues(expectedUOM, _)).reduce { (qtyA, qtyB) =>
        require(qtyA.sum === qtyB.sum, "Cannot merge unequal quantities")
        determineQtyBoundary(qtyA, qtyB, Vector.empty)
      }

      // Apply the common quantity boundaries to the entry's price sequences
      val expandedByQty = groups.map(entries => expandByQty(entries.toList, quantityBoundaries, Vector.empty))
      toSeqCrossSections(expandedByQty)
    }

    groupedByEntry.toSeq.flatten
  }
}

case class CalculationValueSummingCalculationStep(val id: CalculationStepPK, valueNames: Seq[CalculationValueName], mergeCriteria: MergeCriteria) extends AbstractAggregationCalculationStep {
  def getCVPrices(context: CalculationContext) = {
    for {
      priceDoc <- context.priceDocument.toSeq
      valueName <- valueNames
      cv <- priceDoc.getValue(valueName)
    } yield {
      cv.prices
    }
  }

  /** Since this CalculationStep merges prices from multiple CalcualtionValues, the prices of all participating CalculationValues need to be included in the stepView */
  override def enrichContext(context: CalculationContext) = {
    super.enrichContext(context).copyContext(stepView = getCVPrices(context).flatten)
  }

  def groupForMerge(context: CalculationContext) = {
    mergeCriteria.groupByCriteria(context, getCVPrices(context))
  }

  def doMerge(context: CalculationContext, group: Seq[EntryPrice])(implicit converter: Converter[Money, Currency]) = {
    // fold left to update the EntryApplication with new values
    val summedEntryPrice = group.reduce { (a, b) =>
      val unitPrice = for {
        priceA <- a.unitPrice
        priceB <- b.unitPrice
      } yield priceA + priceB

      a.updateEntryPrice(
        startDate = ValidityDates.later(a.startDate, b.startDate),
        endDate = ValidityDates.earlier(a.endDate, b.endDate),
        unitPrice = unitPrice)
    }

    val result = (new EntryApplication(context, summedEntryPrice)).copy(fromEntryPrice = group.map(_.id).flatten)

    if (result.startDate !== ValidityDates.earlier(result.startDate, result.endDate)) {
      val msg = new Message {
        val msg =
          s"""|Cannot return complete pricing when validity dates are invalid.
              |  summed start date: ${result.startDate}
              |  summed end date: ${result.endDate}
              |  grouped prices for merge: ${group}"""
        override def toString = msg
      }
      result.copy(applies = false, messages = msg +: result.messages)
    } else result.copy(applies = true)

  }
}
