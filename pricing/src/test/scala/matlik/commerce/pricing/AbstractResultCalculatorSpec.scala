package matlik.commerce.pricing

import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import matlik.measures._
import matlik.measures.SimpleImplicits._
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.commerce.pk._
import matlik.commerce.pricing.CalculationStep._
import matlik.common.Message

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class AbstractResultCalculatorSpec extends WordSpec with Matchers {
  
  import matlik.measures.SimpleImplicits._
  
  val context = SimpleCalculationContext(stepView = Seq.empty[EntryPrice])
  
  val ep1 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(1))
  val ep2 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(2))
  val ep3 = SimpleEntryPrice(Option(new EntryPriceID()), EntryID(3))
  
  "AbstractResultCalculator" should {
    val dummy = new AbstractResultCalculator() {
      override def doCalculateResults(context: CalculationContext, pairs: Seq[(ApplicationResultGroup, PriceRow)])(implicit converter: Converter[Money, Currency]): Seq[CalculationStepResult] = Seq()
    }
          
    "enforce calculation preconditions" in {
      assertResult(Seq())(dummy.calculateResults(context, Seq.empty, Seq.empty))
      
      intercept[AssertionError] {
        dummy.calculateResults(context, Seq.empty, Seq(SimplePercentPriceRow(10)))
      }
      
      intercept[AssertionError] {
        dummy.calculateResults(context, Seq(Seq(new EntryApplication(context, ep1))), Seq.empty)
      }
    }
    
    "updates results accurately" in {
      val now = DateTime.now()
      val pr1 = SimplePercentPriceRow(10)
      val ea1 = new EntryApplication(context, ep1)
      val price1 = Option(Money(10, USD))
      
      val result1 = dummy.updateResult(ea1, pr1, price1)
      val expected1 = new EntryApplication(context, ep1).copy(
        unitPrice = price1,
        toEntryPrice = result1.toEntryPrice,
        applies = true
      )
      assertResult(expected1)(result1)
      
      val pr2 = pr1.copy(
        startDate = Option(now.minusDays(1)),
        endDate = Option(now.plusHours(1))
      )
      val ea2 = ea1.copy(
        startDate = Option(now.minusHours(1)),
        endDate = Option(now.plusDays(1))
      )
      val result2 = dummy.updateResult(ea2, pr2, price1)
      val expected2 = new EntryApplication(context, ep1).copy(
        unitPrice = price1,
        startDate = ea2.startDate,
        endDate = pr2.endDate,
        toEntryPrice = result2.toEntryPrice,
        applies = true
      )
      assertResult(expected2)(result2)
      
      object Message1 extends Message
      object Message2 extends Message 
      
      val pr3 = SimplePercentPriceRowWithMessage(
        percent = 20,
        messages = Seq(Message1)
      )
      val ea3 = ea1.copy(
        messages = Seq(Message2)  
      )
      val result3 = dummy.updateResult(ea3, pr3, price1)
      val expected3 = new EntryApplication(context, ep1).copy(
        unitPrice = price1,
        toEntryPrice = result3.toEntryPrice,
        // NOTE: The grouper creates the ea with message before price row is applied
        messages = Seq(Message2, Message1),
        applies = true
      )
      assertResult(expected3)(result3)
    }
  }

}