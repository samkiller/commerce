package matlik.commerce.pricing.perf

import org.scalameter.api._
import matlik.commerce.pricing._
import matlik.measures.SimpleImplicits
import scala.util.Random
import org.junit.runner.RunWith

@RunWith(classOf[org.scalatest.junit.JUnitRunner])
class PriceCalculatorCPUBenchmark extends PerformanceTest {

  def reporter = ChartReporter(ChartFactory.XYLine())
  /* configuration */

    lazy val executor = LocalExecutor(
      new Executor.Warmer.Default,
      Aggregator.median,
      new Measurer.Default with Measurer.OutlierElimination)

//  def executor = SeparateJvmsExecutor(
//    new Executor.Warmer.Default,
//    Aggregator.average,
//    new Measurer.Default with Measurer.OutlierElimination)

  //def reporter = new LoggingReporter
  def persistor = Persistor.None
  def historian = Historian.Window

  override def main(args: Array[String]) {
    super.main(args)
  }

  val sizes = Gen.range("size")(30000, 150000, 30000)

  // Generate a sequence of Integers
  val entryIDSequences = for {
    size <- sizes
  } yield {
    Random.setSeed(12345)
    val numOfEntries = math.abs(Random.nextInt % 20) + 1
    val entries = for {
      x <- 0 until numOfEntries
    } yield {
      math.abs(Random.nextInt % 200) + 1
    }
    entries.toSet.toSeq
  }

  performance of "CPUBench" in {
    performance of "PriceCalculator" in {
      measure method "calculate" in {
        using(entryIDSequences) in { entryIdNumbers =>
          val entryIDs = entryIdNumbers.map(EntryID(_))
          val ctx1 = SimpleCalculationContextConverter.build(entryIDs: _*).copy(testLevel = 1)
          val result = PriceCalculatorExamples.pricer.calculate(ctx1)(SimpleImplicits.SimpleConverter)
        }
      }
    }
  }
}

//class PriceCalculatorMemBenchmark extends PerformanceTest {
//
//  def reporter = ChartReporter(ChartFactory.XYLine())
//  /* configuration */
//
//    lazy val executor = LocalExecutor(
//      new Executor.Warmer.Default,
//      Aggregator.average,
//      new Measurer.MemoryFootprint with Measurer.OutlierElimination)
//
////  def executor = SeparateJvmsExecutor(
////    new Executor.Warmer.Default,
////    Aggregator.average,
////    new Measurer.MemoryFootprint)
//
//  //def reporter = new LoggingReporter
//  def persistor = Persistor.None
//  def historian = Historian.Window
//  def measurer = new Executor.Measurer.MemoryFootprint
//
//  override def main(args: Array[String]) {
//    super.main(args)
//  }
//
//  val sizes = Gen.range("size")(30000, 150000, 30000)
//
//  // Generate a sequence of Integers
//  val entryIDSequences = for {
//    size <- sizes
//  } yield {
//    Random.setSeed(12345)
//    val numOfEntries = math.abs(Random.nextInt % 20) + 1
//    val entries = for {
//      x <- 0 until numOfEntries
//    } yield {
//      math.abs(Random.nextInt % 200) + 1
//    }
//    entries.toSet.toSeq
//  }
//
//  performance of "MemoryFootprint" in {
//    performance of "PriceCalculator" in {
//      measure method "calculate" in {
//        using(entryIDSequences) in { entryIdNumbers =>
//          val entryIDs = entryIdNumbers.map(EntryID(_))
//          val ctx1 = SimpleCalculationContextConverter.build(entryIDs: _*).copy(testLevel = 1)
//          val result = PriceCalculatorExamples.pricer.calculate(ctx1)(SimpleImplicits.SimpleConverter)
//        }
//      }
//    }
//  }
//}
