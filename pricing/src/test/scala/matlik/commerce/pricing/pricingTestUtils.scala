package matlik.commerce.pricing

import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.WordSpec
import org.scalatest.Matchers
import org.scalacheck._
import org.scalacheck.Arbitrary._
import org.scalacheck.Gen._
import org.scalacheck.Prop._
import matlik.measures.Money
import matlik.measures.SimpleImplicits._
import matlik.commerce.pk._
import matlik.commerce.Quantity
import matlik.commerce.Quantity._
import matlik.common.Message
import matlik.common.Messages
import matlik.commerce.QUnit
import matlik.core.PKLong

object Const {
  // November 20, 2014 @ 1:00 AM
  val BLACK_FRIDAY = new DateTime(2014, 11, 28, 1, 0)
}

case class EntryID(value: Long) extends EntryPK with PKLong
case class CalculationStepID(value: Long) extends CalculationStepPK

case class SimplePriceDocument(
  id: Option[PriceDocumentID] = None,
  priced: DateTime = Const.BLACK_FRIDAY,
  calculationValues: Seq[CalculationValue] = Seq()) extends PriceDocument {
  def copyPriceDocument(
    id: Option[PriceDocumentID] = id,
    priced: DateTime = priced,
    calculationValues: Seq[CalculationValue] = calculationValues): PriceDocument =
    this.copy(
      id = id,
      priced = priced,
      calculationValues = calculationValues)
}

case class SimpleCalculationContext(
  stepView: Seq[EntryPrice],
  priceDocument: Option[PriceDocument] = None,
  calculationStep: Option[CalculationStepPK] = None,
  calculationDepth: Option[Int] = None,
  testLevel: Int = 999) extends CalculationContext {
  def copyContext(
    view: Seq[EntryPrice],
    priceDocument: Option[PriceDocument],
    calculationStep: Option[CalculationStepPK],
    calculationDepth: Option[Int]): CalculationContext =
    this.copy(
      stepView = view,
      priceDocument = priceDocument,
      calculationStep = calculationStep,
      calculationDepth = calculationDepth)
}

case class SimpleEntryPrice(
  id: Option[EntryPriceID],
  entry: EntryPK,
  quantity: Quantity = Quantity(10, EACH),
  unitPrice: Option[Money] = None,
  startDate: Option[DateTime] = None,
  endDate: Option[DateTime] = None) extends EntryPrice {
  override def updateEntryPrice(
    id: Option[EntryPriceID] = id,
    startDate: Option[DateTime] = startDate,
    endDate: Option[DateTime] = endDate,
    entry: EntryPK = entry,
    quantity: Quantity = quantity,
    unitPrice: Option[Money] = unitPrice): EntryPrice = this.copy(id, entry, quantity, unitPrice, startDate, endDate)
}

case class SimplePercentPriceRow(
  percent: Double,
  id: Option[PriceRowID] = None,
  startDate: Option[DateTime] = None,
  endDate: Option[DateTime] = None,
  minQty: Option[Long] = None,
  maxQty: Option[Long] = None,
  unitOfMeasure: QUnit = EACH) extends PercentPriceRow

case class SimplePercentPriceRowWithMessage(
  percent: Double,
  id: Option[PriceRowID] = None,
  startDate: Option[DateTime] = None,
  endDate: Option[DateTime] = None,
  minQty: Option[Long] = None,
  maxQty: Option[Long] = None,
  unitOfMeasure: QUnit = EACH,
  messages: Seq[Message] = Seq.empty) extends PercentPriceRow with Messages

case class SimpleFixedPriceRow(
  price: Money,
  id: Option[PriceRowID] = None,
  startDate: Option[DateTime] = None,
  endDate: Option[DateTime] = None,
  minQty: Option[Long] = None,
  maxQty: Option[Long] = None,
  unitOfMeasure: QUnit = EACH) extends FixedPriceRow
  
object SimpleCalculationContextConverter {
  def build(entries: EntryID*): SimpleCalculationContext = buildWithSeq(entries.toSeq.map((_, 1)))
  def buildWithSeq(entries: Seq[(EntryID, Int)]): SimpleCalculationContext = {
    var idCounter = 0
    val initialEntryPrices = for { 
      entry <- entries
      (entryId, qty) = entry
    } yield {
      SimpleEntryPrice(Option(new EntryPriceID()), entryId, quantity = Quantity(qty, EACH) )
    }
    SimpleCalculationContext(initialEntryPrices)
  }
}

case class SimpleMessage(text: String) extends Message