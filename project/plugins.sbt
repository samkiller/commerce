//
// Notes:
//  1. This build requires SBT 0.12.x to work properly
//

//============================== 
// Resolvers
//==============================

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += Classpaths.typesafeResolver

// No longer needed with sbt 0.13?
//resolvers += Resolver.url("sbt-plugin-releases",
//    new URL("http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)


//==============================
// Plugins
//==============================

// It is possible to override SBT and Scala versions like this, assuming the plugin is compatible.
//addSbtPlugin("io.spray" % "sbt-twirl" % "0.6.2", sbtVersion = "0.12", scalaVersion = "2.9.2" )

addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.4.0")  // SBT 0.13
//addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.1.1") // SBT 0.12
    
// Twirl templating system: https://github.com/spray/twirl
//addSbtPlugin("io.spray" % "sbt-twirl" % "0.6.1")
